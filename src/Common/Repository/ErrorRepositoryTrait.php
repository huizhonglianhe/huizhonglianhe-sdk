<?php
namespace Sdk\Common\Common\Repository;

use Marmot\Framework\Interfaces\IErrorAdapter;

trait ErrorRepositoryTrait
{
    abstract protected function getAdapter();
    
    public function lastErrorId() : int
    {
        $adapter = $this->getAdapter();
        return $adapter instanceof IErrorAdapter ? $adapter->lastErrorId() : 0;
    }

    public function lastErrorInfo() : array
    {
        $adapter = $this->getAdapter();
        return $adapter instanceof IErrorAdapter ? $adapter->lastErrorInfo() : [];
    }
}
