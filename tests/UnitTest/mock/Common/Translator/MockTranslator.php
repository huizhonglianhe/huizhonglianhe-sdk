<?php
namespace Sdk\Common\Common\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class MockTranslator
{
    use RestfulTranslatorTrait;

    protected function translateToObject(array $expression, $object = null)
    {
    }

    public function publicIncludedAttributes($included)
    {
        return $this->includedAttributes($included);
    }

    public function publicIncludedRelationships($included)
    {
        return $this->includedRelationships($included);
    }

    public function publicRelationship($included, $relationships)
    {
        return $this->relationship($included, $relationships);
    }

    public function publicChangeArrayFormat($relationships)
    {
        return $this->changeArrayFormat($relationships);
    }
}
