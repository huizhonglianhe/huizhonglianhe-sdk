<?php
namespace Sdk\Common\Common\Adapter;

use Marmot\Framework\Interfaces\INull;

class MockAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    protected function getResource()
    {
        return 'test';
    }

    protected function translateToObject($object = null)
    {
    }

    protected function translateToObjects()
    {
    }

    protected function isSuccess() : bool
    {
        return true;
    }

    public function publicGetResource()
    {
        return $this->getResource();
    }

    public function publicFetchOneAsyncAction(int $id)
    {
        return $this->fetchOneAsyncAction($id);
    }

    public function publicFetchListAsyncAction(array $ids)
    {
        return $this->fetchListAsyncAction($ids);
    }

    public function publicSearchAsyncAction(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) {
        return $this->searchAsyncAction($filter, $sort, $number, $size);
    }

    public function publicFetchOneAction(int $id, INull $null)
    {
        return $this->fetchOneAction($id, $null);
    }

    public function publicFetchListAction(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    public function publicSearchAction(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->searchAction($filter, $sort, $number, $size);
    }
}
