<?php
namespace Sdk\Common\Common\Adapter;

use PHPUnit\Framework\TestCase;

class AsyncFetchAbleRestfulAdapterTraitTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockAdapter::class)
                        ->setMethods(
                            [
                                'fetchOneAsyncAction',
                                'getAsync'
                            ]
                        )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testFetchOneAsync()
    {
        $id = 1;
        $expected = [1,2];

        $this->stub = $this->getMockBuilder(MockAdapter::class)
                        ->setMethods(
                            [
                                'fetchOneAsyncAction'
                            ]
                        )->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAsyncAction')
            ->with($id)
            ->willReturn($expected);

        $result = $this->stub->fetchOneAsync($id);

        $this->assertEquals($expected, $result);
    }

    public function testFetchOneAsyncAction()
    {
        $id = 1;
        $expected = [1,2];

        $this->stub = $this->getMockBuilder(MockAdapter::class)
                        ->setMethods(
                            [
                                'getAsync'
                            ]
                        )->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('getAsync')
            ->with($this->stub->publicGetResource().'/'.$id)
            ->willReturn($expected);

        $result = $this->stub->publicFetchOneAsyncAction($id);
        $this->assertEquals($expected, $result);
    }

    public function testFetchListAsync()
    {
        $this->stub = $this->getMockBuilder(MockAdapter::class)
            ->setMethods(
                [
                    'fetchListAsyncAction'
                ]
            )->getMock();

        $ids = array(1,2,3);
        $expected = [1,2];

        $this->stub->expects($this->exactly(1))
            ->method('fetchListAsyncAction')
            ->with($ids)
            ->willReturn($expected);

        $result = $this->stub->fetchListAsync($ids);
        $this->assertEquals($expected, $result);
    }

    public function testFetchListAsyncAction()
    {
        $ids = array(1,2,3);
        $expected = [1,2];

        $this->stub = $this->getMockBuilder(MockAdapter::class)
                        ->setMethods(
                            [
                                'getAsync'
                            ]
                        )->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('getAsync')
            ->with($this->stub->publicGetResource().'/'.implode(',', $ids))
            ->willReturn($expected);

        $result = $this->stub->publicFetchListAsyncAction($ids);
        $this->assertEquals($expected, $result);
    }

    public function testSearchAsync()
    {
        $this->stub = $this->getMockBuilder(MockAdapter::class)
            ->setMethods(
                [
                    'searchAsyncAction'
                ]
            )->getMock();
        $filter = array();
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;
        $expected = [1,2];

        $this->stub->expects($this->exactly(1))
            ->method('searchAsyncAction')
            ->with($filter, $sort, $page, $size)
            ->willReturn($expected);

        $result = $this->stub->searchAsync($filter, $sort, $page, $size);
        $this->assertEquals($expected, $result);
    }

    public function testSearchAsyncAction()
    {
        $filter = array();
        $sort = ['-updateTime'];
        $page = 1;
        $size = 10;
        $expected = [1,2];

        $this->stub = $this->getMockBuilder(MockAdapter::class)
                        ->setMethods(
                            [
                                'getAsync'
                            ]
                        )->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('getAsync')
            ->with($this->stub->publicGetResource())
            ->willReturn($expected);

        $result = $this->stub->publicSearchAsyncAction($filter, $sort, $page, $size);
        $this->assertEquals($expected, $result);
    }
}
