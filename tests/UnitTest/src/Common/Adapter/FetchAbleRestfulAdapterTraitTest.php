<?php
namespace Sdk\Common\Common\Adapter;

use PHPUnit\Framework\TestCase;
use Marmot\Framework\Interfaces\INull;

class FetchAbleRestfulAdapterTraitTest extends TestCase
{
    private $stub;

    private $nullObject;

    private $expectedObject;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockAdapter::class)
                        ->setMethods(
                            [
                                'get',
                                'translateToObject',
                                'translateToObjects',
                                'isSuccess',
                            ]
                        )->getMock();

        $this->nullObject = new class implements INull{
        };
        $this->expectedObject = new class {
        };
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testFetchOne()
    {
        $id = 1;

        $this->stub = $this->getMockBuilder(MockAdapter::class)
                           ->setMethods(
                               ['fetchOneAction']
                           )->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
             ->with($id, $this->nullObject)
            ->willReturn($this->expectedObject);

        $result = $this->stub->fetchOne($id, $this->nullObject);
        $this->assertEquals($this->expectedObject, $result);
    }

    public function testFetchList()
    {
        $ids = [1, 2];
        $expectedArray = ['expected'];

        $this->stub = $this->getMockBuilder(MockAdapter::class)
                           ->setMethods(
                               ['fetchListAction']
                           )->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('fetchListAction')
             ->with($ids)
            ->willReturn($expectedArray);

        $result = $this->stub->fetchList($ids);
        $this->assertEquals($expectedArray, $result);
    }

    public function testSearch()
    {
        $filter = ['filter'];
        $sort = ['sort'];
        $number = 0;
        $size = 10;
        $expectedArray = ['expected'];

        $this->stub = $this->getMockBuilder(MockAdapter::class)
                           ->setMethods(
                               ['searchAction']
                           )->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('searchAction')
             ->with($filter, $sort, $number, $size)
            ->willReturn($expectedArray);

        $result = $this->stub->search($filter, $sort, $number, $size);
        $this->assertEquals($expectedArray, $result);
    }

    public function testfetchOneActionSuccess()
    {
        $id = 1;
        $expectedArray = array('expected');

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with($this->stub->publicGetResource().'/'.$id)
            ->willReturn($expectedArray);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->willReturn($this->expectedObject);

        $result = $this->stub->publicFetchOneAction($id, $this->nullObject);
        $this->assertEquals($this->expectedObject, $result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 1;
        $expectedArray = array('expected');

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with($this->stub->publicGetResource().'/'.$id)
            ->willReturn($expectedArray);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);

        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');

        $result = $this->stub->publicFetchOneAction($id, $this->nullObject);
        
        $this->assertEquals($this->nullObject, $result);
    }

    public function testFetchListActionSuccess()
    {
        $ids = array(1,2,3);
        $expectedResponse = array(
            'response'
        );
        $expectedResult = array(
           ['expectedList'], 1
        );

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with($this->stub->publicGetResource().'/'.implode(',', $ids))
            ->willReturn($expectedResponse);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('translateToObjects')
            ->willReturn($expectedResult);

        $result = $this->stub->publicFetchListAction($ids);
        $this->assertEquals(['expectedList'], $result);
    }

    public function testFetchListActionFailure()
    {
        $ids = array(1,2,3);
        $expectedResponse = array('response');

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with($this->stub->publicGetResource().'/'.implode(',', $ids))
            ->willReturn($expectedResponse);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);

        $this->stub->expects($this->exactly(0))
            ->method('translateToObjects');

        $result = $this->stub->publicFetchListAction($ids);
        
        $this->assertEquals(array(), $result);
    }

    public function testSearchActionSuccess()
    {
        $filter = array('filter');
        $sort = array('sort');
        $number = 0;
        $size = 10;
        $expectedResponse = array('response');
        $expectedResult = array(
           ['expectedList'], 1
        );

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with($this->stub->publicGetResource())
            ->willReturn($expectedResponse);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('translateToObjects')
            ->willReturn($expectedResult);

        $result = $this->stub->publicSearchAction($filter, $sort, $number, $size);
        $this->assertEquals($expectedResult, $result);
    }

    public function testSearchActionFailure()
    {
        $filter = $sort = array();
        $number = 0;
        $size = 10;
        $expectedResponse = array('response');
        
        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with($this->stub->publicGetResource())
            ->willReturn($expectedResponse);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);

        $this->stub->expects($this->exactly(0))
            ->method('translateToObjects');

        $result = $this->stub->publicSearchAction($filter, $sort, $number, $size);
        $this->assertEquals(array(array(), 0), $result);
    }
}
