<?php
namespace Sdk\Common\Common\Translator;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class RestfulTranslatorTraitTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator =  $this->getMockBuilder(MockTranslator::class)
                        ->setMethods(
                            [
                                'translateToObject'
                            ]
                        )->getMock();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testArrayToObjectsWithEmptyArray()
    {
        $expected = array(array(), 0);
        $expression = array();

        $result = $this->translator->arrayToObjects($expression);
        $this->assertEquals($expected, $result);
    }
}
