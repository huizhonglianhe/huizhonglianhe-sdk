<?php
namespace Sdk\Common\Common\Repository;

use Marmot\Framework\Interfaces\IErrorAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class ErrorRepositoryTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockForTrait(ErrorRepositoryTrait::class);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testLastErrorIdSuccess()
    {
        $expected = 1;

        $adapter = $this->prophesize(IErrorAdapter::class);
        $adapter->lastErrorId()->shouldBeCalledTimes(1)->willReturn($expected);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->lastErrorId();
        $this->assertEquals($expected, $result);
    }

    public function testLastErrorIdFail()
    {
        $expected = 0;
        $adapter = new class{
        };

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter);

        $result = $this->stub->lastErrorId();
        $this->assertEquals($expected, $result);
    }

    public function testLastErrorInfoSuccess()
    {
        $expected = ['test'];

        $adapter = $this->prophesize(IErrorAdapter::class);
        $adapter->lastErrorInfo()->shouldBeCalledTimes(1)->willReturn($expected);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->lastErrorInfo();
        $this->assertEquals($expected, $result);
    }

    public function testLastErrorInfoFail()
    {
        $expected = [];
        $adapter = new class{
        };

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter);

        $result = $this->stub->lastErrorInfo();
        $this->assertEquals($expected, $result);
    }
}
