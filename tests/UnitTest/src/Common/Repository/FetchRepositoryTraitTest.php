<?php
namespace Sdk\Common\Common\Repository;

use Sdk\Common\Common\Adapter\IFetchAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class FetchRepositoryTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockForTrait(FetchRepositoryTrait::class);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testFetchOne()
    {
        $id = 1;
        $expected = new class{
        };

        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($expected);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($expected, $result);
    }

    public function testFetchList()
    {
        $ids = [1,2,3];
        $expected = array('expected');

        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1)->willReturn($expected);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->fetchList($ids);
        $this->assertEquals($expected, $result);
    }

    public function testSearch()
    {
        $filter = $sort = array();
        $number = 1;
        $size = 10;

        $expected = array('expected');

        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($number),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn($expected);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->search($filter, $sort, $number, $size);

        $this->assertEquals($expected, $result);
    }
}
