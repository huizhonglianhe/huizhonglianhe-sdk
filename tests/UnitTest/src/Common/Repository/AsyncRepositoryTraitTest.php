<?php
namespace Sdk\Common\Common\Repository;

use Marmot\Framework\Interfaces\IAsyncAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class AsyncRepositoryTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockForTrait(AsyncRepositoryTrait::class);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testFetchOneAsync()
    {
        $id = 1;
        $expected = new class{
        };

        $adapter = $this->prophesize(IAsyncAdapter::class);
        $adapter->fetchOneAsync(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($expected);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->fetchOneAsync($id);
        $this->assertEquals($expected, $result);
    }

    public function testFetchListAsync()
    {
        $ids = [1,2,3];
        $expected = array('expected');

        $adapter = $this->prophesize(IAsyncAdapter::class);
        $adapter->fetchListAsync(Argument::exact($ids))->shouldBeCalledTimes(1)->willReturn($expected);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->fetchListAsync($ids);
        $this->assertEquals($expected, $result);
    }

    public function testSearchAsync()
    {
        $filter = $sort = array();
        $number = 1;
        $size = 10;

        $expected = array('expected');

        $adapter = $this->prophesize(IAsyncAdapter::class);
        $adapter->searchAsync(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($number),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn($expected);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->searchAsync($filter, $sort, $number, $size);

        $this->assertEquals($expected, $result);
    }
}
